package com.nadeem.test_tts_speed;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.EngineInfo;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class TTS_Controller extends Activity implements
		SeekBar.OnSeekBarChangeListener {

	private String SAMPLE_TEXT = "  Alice was beginning to get very tired of sitting by her sister on the bank, and of having nothing to do. Once or twice she had peeped into the book her sister was reading, but it had no pictures or conversations in it, \"and what is the use of a book,\" thought Alice, \"without pictures or conversations?\" \r\n"
			+ "  So she was considering in her own mind (as well as she could, for the day made her feel very sleepy and stupid), whether the pleasure of making a daisy–chain would be worth the trouble of getting up and picking the daisies, when suddenly a White Rabbit with pink eyes ran close by her. \r\n"
			+ "  There was nothing so very remarkable in that, nor did Alice think it so very much out of the way to hear the Rabbit say to itself, \"Oh dear! Oh dear! I shall be too late!\" But when the Rabbit actually took a watch out of its waistcoat–pocket and looked at it and then hurried on, Alice started to her feet, for it flashed across her mind that she had never before seen a rabbit with either a waistcoat–pocket, or a watch to take out of it, and, burning with curiosity, she ran across the field after it and was just in time to see it pop down a large rabbit–hole, under the hedge. In another moment, down went Alice after it! \r\n"
			+ "  The rabbit–hole went straight on like a tunnel for some way and then dipped suddenly down, so suddenly that Alice had not a moment to think about stopping herself before she found herself falling down what seemed to be a very deep well. \r\n"
			+ "  Either the well was very deep, or she fell very slowly, for she had plenty of time, as she went down, to look about her. First, she tried to make out what she was coming to, but it was too dark to see anything; then she looked at the sides of the well and noticed that they were filled with cupboards and book–shelves; here and there she saw maps and pictures hung upon pegs. She took down a jar from one of the shelves as she passed. It was labeled \"ORANGE MARMALADE,\" but, to her great disappointment, it was empty; she did not like to drop the jar, so managed to put it into one of the cupboards as she fell past it. \r\n"
			+ "  Down, down, down! Would the fall never come to an end? There was nothing else to do, so Alice soon began talking to herself. \"Dinah'll miss me very much to–night, I should think!\" (Dinah was the cat.) \"I hope they'll remember her saucer of milk at tea–time. Dinah, my dear, I wish you were down here with me!\" Alice felt that she was dozing off, when suddenly, thump! thump! down she came upon a heap of sticks and dry leaves, and the fall was over. ";

	double newpitch = 1.0f, newspeed = 1.0f;

	TextToSpeech tts, newTts;
	ArrayAdapter<String> adapter_Engines;
	List<String> engines;
	ListView lv_Engines;
	List<EngineInfo> list_Engines;

	TextView tv_Speed, tv_Pitch;

	SeekBar sb_Speed, sb_Pitch;

	Button btn_Speak, btn_Download;

	HashMap<String, Float> hashMap_Speed, hashMap_Pitch;

	private ProgressDialog pd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tts__controller);

		engines = new ArrayList<String>();
		lv_Engines = (ListView) findViewById(R.id.lv_Engines);

		sb_Pitch = (SeekBar) findViewById(R.id.sb_Pitch);
		sb_Speed = (SeekBar) findViewById(R.id.sb_Speed);

		tv_Speed = (TextView) findViewById(R.id.tv_Speed);
		tv_Pitch = (TextView) findViewById(R.id.tv_Pitch);

		sb_Speed.incrementProgressBy(1 * 10);
		sb_Speed.setMax(19);

		sb_Pitch.incrementProgressBy(1 * 10);
		sb_Pitch.setMax(39);

		sb_Pitch.setOnSeekBarChangeListener(this);
		sb_Speed.setOnSeekBarChangeListener(this);

		sb_Pitch.setProgress(10);
		sb_Speed.setProgress(10);

		btn_Speak = (Button) findViewById(R.id.btn_SpeakSample);
		btn_Download = (Button) findViewById(R.id.btn_Download);

		btn_Speak.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				tts.setPitch((float) newpitch);
				tts.setSpeechRate((float) newspeed);

				tts.speak(SAMPLE_TEXT, TextToSpeech.QUEUE_FLUSH, null);
			}
		});

		btn_Download.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				downloadVoiceBundle();
			}
		});

		Intent intent = new Intent();
		intent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
		startActivityForResult(intent, 0);
	}

	void downloadVoiceBundle() {
		Intent installTTSIntent = new Intent();
		installTTSIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
		ArrayList<String> languages = new ArrayList<String>();
		installTTSIntent.putStringArrayListExtra(
				TextToSpeech.Engine.EXTRA_CHECK_VOICE_DATA_FOR, languages);
		startActivity(installTTSIntent);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 0) {
			if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
				Toast.makeText(getApplicationContext(),
						"Text to Speech Engine Installed", Toast.LENGTH_LONG)
						.show();

				init();
			} else {
				Intent installIntent = new Intent();
				installIntent
						.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
				startActivity(installIntent);
				Toast.makeText(getApplicationContext(), "Install Now",
						Toast.LENGTH_LONG).show();
			}
		}
	}

	void init() {
		Toast.makeText(
				getApplicationContext(),
				"Press 'Speak Sample Text' button after making change in TTS engine, Speech, or Rate",
				Toast.LENGTH_LONG).show();

		showDialog();
		tts = new TextToSpeech(getApplicationContext(), new OnInitListener() {

			@Override
			public void onInit(int status) {
				// TODO Auto-generated method stub
				Log.e("TAG", "TTS Initialized");

				cancelDialog();

				list_Engines = tts.getEngines();

				for (int i = 0; i < list_Engines.size(); i++) {
					EngineInfo engine = list_Engines.get(i);

					engines.add(engine.label);
				}

				adapter_Engines = new ArrayAdapter<String>(
						getApplicationContext(),
						android.R.layout.simple_list_item_1, engines);

				lv_Engines.setEmptyView(findViewById(R.id.emptyView));
				lv_Engines.setAdapter(adapter_Engines);

				try {
					Locale l = tts.getLanguage();
					Toast.makeText(
							getApplicationContext(),
							"Current selected Language is : "
									+ l.getDisplayCountry(), Toast.LENGTH_LONG)
							.show();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

		lv_Engines.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub

				showDialog();
				newTts = new TextToSpeech(getApplicationContext(),
						new OnInitListener() {

							@Override
							public void onInit(int arg0) {
								// TODO Auto-generated method stub

								cancelDialog();
								tts.shutdown();
								tts = newTts;
								// newTts.shutdown();

								btn_Speak.performClick();
							}
						}, list_Engines.get(arg2).name);
			}
		});

	}

	void showDialog() {
		try {
			pd = new ProgressDialog(TTS_Controller.this);
			pd.setMessage("Please Wait...");
			pd.setCancelable(false);
			pd.setCanceledOnTouchOutside(false);
			pd.show();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	void cancelDialog() {
		try {
			pd.cancel();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void installVoiceData() {
		Intent intent = new Intent(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.setPackage("com.google.android.tts"/*
												 * replace with the package name
												 * of the target TTS engine
												 */);
		try {
			Log.e("TAG", "Installing voice data: " + intent.toUri(0));
			startActivity(intent);
		} catch (ActivityNotFoundException ex) {
			Log.e("TAG", "Failed to install TTS data, no acitivty found for "
					+ intent + ")");
		}
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		// TODO Auto-generated method stub

		Log.e("Progress", "" + progress);

		if (seekBar == sb_Pitch) {
			newpitch = ((double) progress + 1) / 10;

			tv_Pitch.setText("Pitch = " + newpitch);
		} else if (seekBar == sb_Speed) {
			newspeed = ((double) progress + 1) / 10;

			tv_Speed.setText("Speed = " + newspeed);
		}
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();

		try {
			newTts.shutdown();
			tts.shutdown();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
